import React, { component, useState } from "react";

function SearchInput ({ id, type, placeholder,onChange }) {
  return (
    <form>
      <input id={id} type={type} className={"nom"} placeholder={placeholder} onChange={onChange}/>
    </form>
  );
}

export default SearchInput;
