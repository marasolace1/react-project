import React, { useState } from "react";
import ImageArray from "../Utils/imageArray";
import {
  Img,
  H3,
  Button,
  Form,
  Image,
  Drawer,
  SecondWrapper,
  MainImageWrapper,
} from "./styledImages";
import Header from "../Header";
import Input from "../SearchBar";

const Second = () => { //rename this app 
  const [searchText, setSearchText] = useState("");

  // this state stores the images uploaded through the input element
  const [incomingImage, setIncomingImage] = useState("");

  // this state is an array, it holds all the displayed data
  const [imageArray, setImageArray] = useState(ImageArray);


  const searchedItem = imageArray.filter(({imageId}) =>
  imageId.toLowerCase().includes(searchText.toLowerCase())
);

// this function get the image file and store it in the incomingImage state 
  const handleUploadImage = ({ target: { files } }) => {
    if (files && files[0]) {
      setIncomingImage(URL.createObjectURL(files[0]));
      console.log(files[0]);
    }
  };

// this function get the image from incoming image state and add it to the imageArray state
  const handleUploadImageButton = (e) => {
    e.preventDefault();
    setImageArray((prevState) => [
      ...prevState,
      {
        imageName: "img2", 
        imagePath: incomingImage,
        imageId: "street" //this value should be dynamic, create an input that collect data for this
      },
    ]);
  };
  // ------------------------------------

 
  return (
    <MainImageWrapper>
      {/* HEADER SECTION(section one) */}
      <Header
        id="search"
        type="text"
        placeholder="Search For Images Here"
        onChange={(e) => setSearchText(e.target.value)}
      />

{/* SECTION TWO (drswer and image display) */}
      <SecondWrapper>
        <Drawer>

          {/* -------------------------------------------- */}
          <div className="file-upload">
            <h2>Add images here</h2>
            <Form>
              {/* style this input */}
              <Input id="nom" type="file" onChange={handleUploadImage} /> 

              <Button onClick={handleUploadImageButton}>Add</Button>
            </Form>
          </div>
          {/* --------------------------------------------- */}
          <div className="ul-div">
            <ul>
              <li>Login/Sign-up</li>
              <li>Attribute An Author</li>
              <li>Contact US</li>
              <li>FAQ</li>
            </ul>
          </div>
        </Drawer>
        <div className="imageDiv">
          <Image>
            {searchedItem.map(({ imageId, imagePath }, idx) => (
              <div className="image-set">
                <div className ="image">
                  <Img key={idx + imageId + imagePath} src={imagePath} />
                </div>
                <H3>{imageId}</H3>
              </div>
            ))}
          </Image>
        </div>
      </SecondWrapper>
    </MainImageWrapper>
  );
};

export default Second;
