import styled from "styled-components";

export const MainImageWrapper = styled.div`
  height: 100vh;
`;
export const SecondWrapper = styled.div`
  height: 100%;
  display: flex;
  background-color: wheat;

  body {
    overflow-y: hidden;
  }

  .imageDiv {
    width: 100%;
    height: 100vh;
    overflow-y: scroll;
  }

  .image-set {
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: space-evenly;
  }

  .image {
    width: 250px;
    height: 250px;
  }
`;

export const Drawer = styled.div`
  display: flex;
  flex-direction: column;
  flex-shrink: 2;
  flex: 1;
  background-color: beige;
  min-width: 300px;


  h2 {
    margin-left: 15px;
  }

  ul {
    color: grey;
    list-style: none;
    margin: none;
    /* min-width: 100px; */
  }

  li {
    cursor: pointer;
    padding: 20px 0px;
    margin-right: 20px;
    border-bottom: 2px solid grey;
    :hover {
      box-shadow: 10px 10px 20px burlywood;
    }
  }
`;

export const Image = styled.div`
  flex: 4;
  /* width: 85%; */
  display: flex;
  flex-wrap: wrap;
  padding-top: 50px;
  justify-content: space-around;
`;

export const Img = styled.img`
  src: url(${(props) => props.src});
  /* margin: 15px; */
  height: 100%;
  cursor: pointer;
  width: 100%;
  border-radius: 5px;

  :hover {
    transition: 0.5s;
    box-shadow: 10px 10px 20px burlywood;
  }
`;

export const Form = styled.form`
  display: flex;
  margin: 0px 20px;
  flex-direction: column;

`;

export const Button = styled.button`
  border: none;
  width: 100px;
  padding: 5px;
  cursor: pointer;
  margin: 20px 0px;
  border-radius: 3px;
  background-color: goldenrod;
  color: (${(props) => props.color});
  
  :focus{
    outline: 0;
  }
`;

export const H1 = styled.h1`
  /* justify-self: center; */
  margin: auto;
  color: beige;
`;

export const H3 = styled.h3`
  margin: 20px 0px;
  color: grey;
`;
