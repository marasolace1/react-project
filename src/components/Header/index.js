import React from 'react';
import SearchInput from '../SearchBar'
import {Div, H1 } from './styledHeader'

const Header = ({id, type, placeholder, onChange}) => {
  return (
    <Div>
      <H1>MY GALLERY</H1>
      <SearchInput 
        id={id}
        type={type} 
        placeholder={placeholder}
        onChange={onChange}
      />
    </Div>
  );
};

export default Header