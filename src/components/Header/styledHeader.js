import styled from 'styled-components'

export const Div = styled.header`
  display: flex; 
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
  background-color: goldenrod;
  /* padding: 3px 20px; */
  

  input {
    margin: 10px;
    padding: 10px;
    border-radius: 5px;
    border: none;
    min-width: 400px;

    :focus{
    outline: 0;
  }
  
  }
`

export const H1 = styled.h1`
  /* margin: 10px; */
  color: beige;
`