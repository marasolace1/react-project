import React from "react";
import File from "./SearchBar";
import { Button, Form } from "../components/ImageContainer/styledImages";
import {H3 } from './Header/styledHeader'

const Third = () => {
  return (
    <div>
      <H3>Add images here</H3>
      <Form>
        <File id="nom" type="file" />
        <Button>Add</Button>
      </Form>
    </div>
  );
};

export default Third;
