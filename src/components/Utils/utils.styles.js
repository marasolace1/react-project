import styled from 'styled-components'

export const Form = styled.form`
  display: flex;
  margin: 0px 20px;
  flex-direction: column;
`;

export const Button = styled.button`
  border: none;
  width: 100px;
  padding: 5px;
  cursor: pointer;
  margin: 20px 0px;
  border-radius: 3px;
  background-color: goldenrod;
  color: (${(props) => props.color});
`;

export const H1 = styled.h1`
  /* justify-self: center; */
  margin: auto;
  color: beige;
`

export const H3 = styled.h3`
  margin: 20px 20px;
  color: grey;
`
// export const Input = styled.input`
//   margin: auto;
//   padding: 10px;
// `