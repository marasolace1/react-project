 import React from 'react';

 import image2 from "../../images/img2.jpg";
import image1 from "../../images/img1.jpg";
import image3 from "../../images/img3.jpg";
import image4 from "../../images/img4.jpg";
import image5 from "../../images/img5.jpg";
import image6 from "../../images/img6.jpg";
import image7 from "../../images/img7.jpg";
import image8 from "../../images/img8.jpg";
import image9 from "../../images/img9.jpg";
import image10 from "../../images/img10.jpg";
// import image11 from "../../images/img11.jpg";
import image12 from "../../images/img12.jpg";
import image13 from "../../images/img13.jpg";
import image14 from "../../images/img14.jpg";
import image15 from "../../images/img15.jpg";
 
 const ImageArray = [
  {
    imageName: "img1",
    imagePath: image1,
    imageId: "lovers",
  },
  {
    imageName: "img2",
    imagePath: image2,
    imageId: "street",
  },
  {
    imageName: "img3",
    imagePath: image3,
    imageId: "workspace",
  },
  {
    imageName: "img4",
    imagePath: image4,
    imageId: "developer",
  },
  {
    imageName: "img5",
    imagePath: image5,
    imageId: "glass house",
  },
  {
    imageName: "img6",
    imagePath: image6,
    imageId: "laptop",
  },
  {
    imageName: "img7",
    imagePath: image7,
    imageId: "laptops",
  },
  {
    imageName: "img8",
    imagePath: image8,
    imageId: "coffee",
  },
  {
    imageName: "img9",
    imagePath: image9,
    imageId: "work tools",
  },
  {
    imageName: "img10",
    imagePath: image10,
    imageId: "subway",
  },
  {
    imageName: "img12",
    imagePath: image12,
    imageId: "tablet",
  },
  {
    imageName: "img13",
    imagePath: image13,
    imageId: "house",
  },
  {
    imageName: "img14",
    imagePath: image14,
    imageId: "museum",
  },
  {
    imageName: "img15",
    imagePath: image15,
    imageId: "morning coffee",
  },
];

export default ImageArray