import React, { useState } from 'react'
import Input from '../SearchBar'
import {H3, Button, Form } from './utils.styles'
import ImageArray from './imageArray'


export const UpdateImageArray = () => {
  const [imageArray, setImageArray] = useState(ImageArray)

  const handleUploadImage = ({target:{files}}) => {
    if (files && files[0]) {
      let img = files[0];
      let pic = URL.createObjectURL(img)

      setImageArray((prevState) => [
        ...prevState,
        {
          imageName: "img2",
          imagePath: pic,
          imageId: "street",
        }
        ])
    }
  }


  return (
    <div>
      <H3>Add images here</H3>
      <Form>
        <Input 
        id="nom" 
        type="file" 
        onChange={handleUploadImage}
         />

        <Button
        onClick = {handleUploadImage}
        >
          Add
          </Button>
          <div style={{height:"200px", width:"200px"}}>
           { imageArray.map(({imagePath, imageId}) => (
             <>
          <img 
          style={{
            height:"20%", 
            width:"20%", 
            margin:'0 5px'}} 
            src = {imagePath} />
          <h2>{imageId}</h2>
          </>
            ))}

          {/* {imageArray.map(({imageId}) => {
            return <div>{imageId}</div>
          })} */}

          </div>
      </Form>
    </div>
  );
};

// export const ImageArray = imageArray